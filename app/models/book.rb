class Book < ApplicationRecord
    belongs_to :category
    belongs_to :user
    has_many :reviews
    
    
    validates :title, presence: true, length: {maximum: 30}
    validates :description, presence: true
    
    validates :author, presence: true, length: {maximum: 30}
    
    
  has_attached_file :book_img, styles: { :book_index => "250x350>", :book_show => "325x475>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :book_img, content_type: /\Aimage\/.*\z/

end
